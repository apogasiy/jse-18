package main.java.com.tsc.apogasiy.tm.command.project;

import main.java.com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-update-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateById(id, name, description);
    }

}