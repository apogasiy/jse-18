package main.java.com.tsc.apogasiy.tm.command.user;

import main.java.com.tsc.apogasiy.tm.command.AbstractUserCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-change-password";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change user password";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth)
            throw new AccessDeniedException();
        System.out.println("Enter id: ");
        final String userId = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findUserById(userId);
        if (user == null)
            throw new UserNotFoundException();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (currentUserId.equals(user.getId()))
            throw new AccessDeniedException();
        System.out.println("Enter password: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
