package main.java.com.tsc.apogasiy.tm.command.system;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println("1.17.0");
    }

}
