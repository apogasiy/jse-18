package main.java.com.tsc.apogasiy.tm.command;

import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyNameException;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand{

    protected void showProject(Project project) {
        if (project == null)
            throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return new Project(name, description);
    }

}
