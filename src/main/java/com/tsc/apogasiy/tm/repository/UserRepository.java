package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.IUserRepository;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> userList = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return userList;
    }

    @Override
    public void add(User user) {
        userList.add(user);
    }

    @Override
    public void remove(User user) {
        userList.remove(user);
    }

    @Override
    public void clear() {
        userList.clear();
    }

    @Override
    public User findUserById(String id) {
        for (User user : userList) {
            if (id.equals(user.getId()))
                return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User findUserByLogin(String login) {
        for (User user : userList) {
            if (login.equals(user.getLogin()))
                return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User findUserByEmail(String email) {
        for (User user : userList) {
            if (email.equals(user.getEmail()))
                return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeUserById(String id) {
        final User user = findUserById(id);
        remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(String login) {
        final User user = findUserByLogin(login);
        remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(String email) {
        return findUserByEmail(email) != null;
    }

}
