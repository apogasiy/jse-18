package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IAuthRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.IAuthService;
import main.java.com.tsc.apogasiy.tm.api.repository.IUserService;
import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyIdException;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyLoginException;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyPasswordException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IAuthRepository authRepository;
    private final IUserService userService;

    public AuthService(final IAuthRepository authRepository, final IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getCurrentUserId() {
        final String userId = authRepository.getCurrentUserId();
        if (userId == null)
            throw new EmptyIdException();
        return userId;

    }

    @Override
    public void setCurrentUserId(String userId) {
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public boolean isAuth() {
        final String currentUserId = authRepository.getCurrentUserId();
        return !(currentUserId == null || currentUserId.isEmpty());
    }

    @Override
    public boolean isAdmin() {
        final String userId = getCurrentUserId();
        final Role role = userService.findUserById(userId).getRole();
        return role.equals(Role.ADMIN);
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        if (login == password || password.isEmpty())
            throw new EmptyPasswordException();
        final User user = userService.findUserByLogin(login);
        if (user == null)
            throw new UserNotFoundException();
        final String hash = HashUtil.encrypt(password);
        if (hash == null || !hash.equals(user.getPassword()))
            throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (!isAuth())
            throw new AccessDeniedException();
        setCurrentUserId(null);
    }
}
