package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IUserRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.IUserService;
import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.exception.empty.*;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserEmailExistsException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserLoginExistsException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void add(User user) {
        userRepository.add(user);
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        if (isLoginExists(login))
            throw new UserLoginExistsException(login);
        final User user = new User(login, HashUtil.encrypt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(String login, String password, String email) {
        User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(String userId, String password) {
        if (userId == null || userId.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        final User user = findUserById(userId);
        if (user == null)
            throw new UserNotFoundException();
        user.setPassword(HashUtil.encrypt(password));
        return user;
    }

    @Override
    public User setRole(String userId, Role role) {
        if (userId == null || userId.isEmpty())
            throw new EmptyLoginException();
        if (role == null)
            throw new EmptyRoleException();
        final User user = findUserById(userId);
        if (user == null)
            throw new UserNotFoundException();
        user.setRole(role);
        return user;

    }

    @Override
    public void remove(User user) {
        userRepository.remove(user);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public User findUserById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return userRepository.findUserById(id);
    }

    @Override
    public User findUserByLogin(String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User findUserByEmail(String email) {
        if (email == null || email.isEmpty())
            throw new EmptyEmailException();
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User removeUserById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return userRepository.removeUserById(id);
    }

    @Override
    public User removeUserByLogin(String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        return userRepository.findUserByLogin(login);
    }

    @Override
    public boolean isLoginExists(String login) {
        return userRepository.findUserByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(String email) {
        return userRepository.findUserByEmail(email) != null;
    }

    @Override
    public User updateUserById(String id, String lastName, String firstName, String middleName, String email) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty())
            throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty())
            throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty())
            throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty())
            throw new EmptyEmailException();
        if (isEmailExists(email))
            throw new UserEmailExistsException(email);
        final User user = findUserById(id);
        if (user == null)
            throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateUserByLogin(String login, String lastName, String firstName, String middleName, String email) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();
        if (isLoginExists(email))
            throw new UserLoginExistsException(login);
        if (lastName == null || lastName.isEmpty())
            throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty())
            throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty())
            throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty())
            throw new EmptyEmailException();
        final User user = findUserByLogin(login);
        if (user == null)
            throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

}
