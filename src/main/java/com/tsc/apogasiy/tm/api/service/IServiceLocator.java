package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IAuthService;
import main.java.com.tsc.apogasiy.tm.api.repository.IUserService;

public interface IServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

}
