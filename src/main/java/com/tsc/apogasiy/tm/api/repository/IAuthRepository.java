package main.java.com.tsc.apogasiy.tm.api.repository;

public interface IAuthRepository {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

}
