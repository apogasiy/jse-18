package main.java.com.tsc.apogasiy.tm.exception.empty;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error! Index is empty!");
    }

}
