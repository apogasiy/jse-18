package main.java.com.tsc.apogasiy.tm.exception.system;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String command) {
        super("Incorrect command:'" + command + "'");
    }

}
