package main.java.com.tsc.apogasiy.tm.exception.entity;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class UserEmailExistsException extends AbstractException {

    public UserEmailExistsException(final String email) {
        super("Error! User with email '" + email + "' already exists.");
    }

}
